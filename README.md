## Django Request Tree

This module provides a decorator for your django views which will take the flat
django request response and convert the POST and FILES into a tree structure.

This tree can optionally be sent to be varified using an xsd like data-validator
if installed.

## Data Example

For example, your html web page might provide a way for javascript to generate
many input widgets and for these to be numbered and collated, once complete the
html form submits this data:

```
  fruit_0_name="banana"
  fruit_0_colour="yellow"
  fruit_1_name="apple"
  fruit_1_colour="red"
  bread_name="tigle"
  bread_type="hairy"
```

Into this:

```
  {
    'fruit': [
      { 'name': "banana", 'colour': "yellow" },
      { 'name': "apple", 'color': "red" },
    ],
    'bread': { 'name': "tigle", 'type': "hairy" },
  }
```

## Use Example

```
 import request_tree

 @request_tree.generate
 def example_view(request):
     print request.TREE
```

## Validation Examples

```
 import request_tree

 schema = {
   'root': {'name': 'user', 'type': 'user'},
   'complexTypes': {
     'user': [
       {'name': 'username', 'maxLength': 50, 'pattern': '[a-zA-Z1-9_\-]+'},
     ]
   }
 }

 @request_tree.validate(schema=MY_INPUT_SCHEMA)
 def example_view(request):
     print request.VALUS
```

See the documentation on the python xds validator module for details about how to construct a schema.

## Bug Reports and Development

Please report any problems to the GitLab Issues tracker.

This code is still a work in progress, please submit your ideas and patches directly to me via email.
